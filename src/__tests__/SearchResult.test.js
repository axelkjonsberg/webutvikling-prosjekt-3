import React from "react";
import ReactDOM, { unmountComponentAtNode } from "react-dom";
import { getByText } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import SearchResult from "../components/Search/SearchResult";
import { MemoryRouter } from "react-router-dom";

let container = null;
beforeEach(() => {
	container = document.createElement("div");
	document.body.appendChild(container);
});

afterEach(() => {
	unmountComponentAtNode(container);
	container.remove();
	container = null;
});

it("renders without crashing", () => {
	ReactDOM.render(
		<MemoryRouter>
			<SearchResult result={{ _id: "fakeid", name: "fakename", minutes: 60 }} />
		</MemoryRouter>,
		container
	);
});

it("displays recipe name", () => {
	act(() => {
		ReactDOM.render(
			<MemoryRouter>
				<SearchResult
					result={{ _id: "fakeid", name: "fakename", minutes: 60 }}
				/>
			</MemoryRouter>,
			container
		);
	});
	expect(getByText(container, "fakename")).toBeDefined();
});

it("formats 60 mins correctly", () => {
	act(() => {
		ReactDOM.render(
			<MemoryRouter>
				<SearchResult
					result={{ _id: "fakeid", name: "fakename", minutes: 60 }}
				/>
			</MemoryRouter>,
			container
		);
	});
	expect(getByText(container, "1 hour")).toBeDefined();
});

it("formats 90 mins correctly", () => {
	act(() => {
		ReactDOM.render(
			<MemoryRouter>
				<SearchResult
					result={{ _id: "fakeid", name: "fakename", minutes: 90 }}
				/>
			</MemoryRouter>,
			container
		);
	});
	expect(getByText(container, "1 hour, 30 minutes")).toBeDefined();
});

it("formats 1440 mins correctly", () => {
	act(() => {
		ReactDOM.render(
			<MemoryRouter>
				<SearchResult
					result={{ _id: "fakeid", name: "fakename", minutes: 1440 }}
				/>
			</MemoryRouter>,
			container
		);
	});
	expect(getByText(container, "1 day")).toBeDefined();
});

it("formats 2200 mins correctly", () => {
	act(() => {
		ReactDOM.render(
			<MemoryRouter>
				<SearchResult
					result={{ _id: "fakeid", name: "fakename", minutes: 2200 }}
				/>
			</MemoryRouter>,
			container
		);
	});
	expect(getByText(container, "1 day, 12 hours")).toBeDefined();
});
