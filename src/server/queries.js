//const fetch = require("node-fetch");

/**
 * Standard form of graphQL queries. It always needs the specified headers and a query string
 * @param {string} query A graphQL-formatted query string. Experiment with graphiql to see what is accepted
 * @returns {Promise<Object>} A json object with the desired fields
 */
export async function queryGraphql(query) {
	let graphQlURL = "http://it2810-25.idi.ntnu.no:4000/graphql";
	if (process.env.NODE_ENV && process.env.NODE_ENV === "development") {
		graphQlURL = "http://localhost:4000/graphql";
	}
	const data = await fetch(graphQlURL, {
		method: "POST",
		headers: {
			"Content-Type": "application/json",
			"Accept": "application/json",
			"Authorization": localStorage.getItem("token"),
		},
		body: JSON.stringify({ query }),
	});
	return await data.json();
}

export async function fetchUsers() {
	const query = `
		query {
			users {
				username
			}
		}
  `;
	return await queryGraphql(query);
}

export async function registerUser(username, password, confirmPassword) {
	const query = `
		mutation {
			register(
				registerInput: {
					username: "${username}" 
					password: "${password}" 
					confirmPassword: "${confirmPassword}"
				}
				) {
					_id
					username
					token
			}
		}
	`;
	return await queryGraphql(query);
}

export async function loginUser(username, password) {
	const query = `
		mutation {
			login(
				username: "${username}" 
				password: "${password}" 
			) {
				_id
				username
				token
			}
		}
	`;
	return await queryGraphql(query);
}

export async function getUserByID(userID) {
	const query = `
		query {
			getUser(userID: "${userID}"){
				_id
				username
				createdAt
				token
			}
		}
	`;
	return await queryGraphql(query);
}

export async function authenticated() {
	const query = `
		query {
			authenticated {
				_id
				username
			}
		}
	`;
	return await queryGraphql(query);
}

export async function getSearchHistory(userID) {
	const query = `
		query {
			getSearchHistory${userID ? `(userID: "${userID}")` : ""} {
				searchValue
				createdAt
			}
		}
	`;
	return await queryGraphql(query);
}

export async function addHistory(searchValue) {
	const query = `
		mutation {
			addHistory(searchValue: "${searchValue}"){
				searchValue
				createdAt
			}
		}
	`;
	return await queryGraphql(query);
}

/**
Creates a recipe-entry and returns the desired fields.
All fields of recipe are required.
Returns an object with the same properties as desiredFields
@param {Object} recipe { name: string, minutes: int, tags: [string], description: string,  steps: [string], ingredients: [string] }
@param {Object} desiredFields { _id: bool, name: bool, minutes: bool, tags: bool, description: bool, steps: bool, n_steps: bool, ingredients: bool, n_ingredients: bool }
@returns {Promise<Object>} promise of object with the same properties as desiredFields
*/
export async function createRecipe(recipe, desiredFields) {
	const query = `
		mutation {
				createRecipe(
					name: "${recipe.name}" 
					minutes: ${recipe.minutes} 
					description: "${recipe.description}"
					tags: [${recipe.tags.map(tag => `"${tag}",`)}] 
					steps: [${recipe.steps.map(step => `"${step}",`)}] 
					ingredients: [${recipe.ingredients.map(ingredient => `"${ingredient}",`)}]
				) {
					${desiredFields._id ? "_id" : ""}
					${desiredFields.name ? "name" : ""}
					${desiredFields.minutes ? "minutes" : ""}
					${desiredFields.tags ? "tags" : ""}
					${desiredFields.description ? "description" : ""}
					${desiredFields.steps ? "steps" : ""}
					${desiredFields.n_steps ? "n_steps" : ""}
					${desiredFields.ingredients ? "ingredients" : ""}
					${desiredFields.n_ingredients ? "n_ingredients" : ""}
				}
		}
	`;
	console.log(query);
	return await queryGraphql(query);
}

export async function deleteRecipe(id) {
	const query = `
		mutation {
			deleteRecipe(recipeID: "${id}")
		}
	`;
	return await queryGraphql(query);
}

/**
 * Gets the recipe with the specified id with all fields included
 * @param {string} id
 * @returns {Promise<Object>}
 */
export async function getRecipe(id) {
	const query = `
		query {
			recipe(recipeID: "${id}") {
				_id
				name
				minutes
				tags
				description
				steps
				n_steps
				ingredients
				n_ingredients
			}
		}
	`;
	return await queryGraphql(query);
}

/**
 * Query for getting the recipe you just created.
 * Necessary because mongoose will not return a created document.
 * @param {Object} recipe
 */
export async function getClosestRecipeID(recipe) {
	const query = `
		query {
			closestRecipe(
				name: "${recipe.name}" 
				minutes: ${recipe.minutes} 
				description: "${recipe.description}"
				tags: [${recipe.tags.map(tag => `"${tag}",`)}] 
				steps: [${recipe.steps.map(step => `"${step}",`)}] 
				ingredients: [${recipe.ingredients.map(ingredient => `"${ingredient}",`)}]
			) {
				_id
			}
		}
	`;
	return await queryGraphql(query);
}

/**
 * Searches recipes with all specified conditions and returns the desired fields.
 * Sortfield can be any field desiredFields may contain.
 * Searching for a phrase can be done by adding \" before and after the searchValue
 * All properties except searchValue are optional.
 * @param {Object} search { searchValue: string, start: int, sortField: string, sortDESC: bool, minMinutes: int, maxMinutes: int, minIngredients: int, maxIngredients: int, minSteps: int, maxSteps: int}
 * @param {Object} desiredFields { _id: bool, name: bool, minutes: bool, tags: bool, description: bool, steps: bool, n_steps: bool, ingredients: bool, n_ingredients: bool }
 * @returns {Promise<Object>} promise of object with the same properties as desiredFields
 */
export async function searchRecipes(search, desiredFields) {
	const query = `
		query {
			recipes(
				${`searchValue: "${search.searchValue}"`}
				${search.start ? `, start: ${search.start}` : ""}
				${search.limit ? `, limit: ${search.limit}` : ""}
				${search.sortField ? `, sortField: "${search.sortField}"` : ""}
				${search.sortDESC ? `, sortDESC: ${search.sortDESC}` : ""}
				${search.minMinutes ? `, minMinutes: ${search.minMinutes}` : ""}
				${search.maxMinutes ? `, maxMinutes: ${search.maxMinutes}` : ""}
				${search.minIngredients ? `, minIngredients: ${search.minIngredients}` : ""}
				${search.maxIngredients ? `, maxIngredients: ${search.maxIngredients}` : ""}
				${search.minSteps ? `, minSteps: ${search.minSteps}` : ""}
				${search.maxSteps ? `, maxSteps: ${search.maxSteps}` : ""}
			) {
				${desiredFields._id ? "_id" : ""}
				${desiredFields.name ? "name" : ""}
				${desiredFields.minutes ? "minutes" : ""}
				${desiredFields.tags ? "tags" : ""}
				${desiredFields.description ? "description" : ""}
				${desiredFields.steps ? "steps" : ""}
				${desiredFields.n_steps ? "n_steps" : ""}
				${desiredFields.ingredients ? "ingredients" : ""}
				${desiredFields.n_ingredients ? "n_ingredients" : ""}
			}
		}
	`;
	return await queryGraphql(query);
}

export async function fetchWordcloud() {
	const query = `
		query {
			wordcloud {
				word
				count
			}
		}
	`;
	return await queryGraphql(query);
}

// module.exports.searchRecipes = searchRecipes;
// module.exports.getClosestRecipeID = getClosestRecipeID;
// module.exports.getRecipe = getRecipe;
// module.exports.addHistory = addHistory;
// module.exports.getSearchHistory = getSearchHistory;
// module.exports.createRecipe = createRecipe;
// module.exports.deleteRecipe = deleteRecipe;
// module.exports.registerUser = registerUser;
// module.exports.loginUser = loginUser;
// module.exports.authenticated = authenticated;
// module.exports.fetchUsers = fetchUsers;
// module.exports.queryGraphql = queryGraphql;
// module.exports.fetchWordcloud = fetchWordcloud;
// module.exports.getUserByID = getUserByID;
