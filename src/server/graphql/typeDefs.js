const gql = require("graphql-tag");

module.exports = gql`

	type CountedWord {
		word: String!
		count: Int!
	}

	type Recipe {
		_id: ID!
		name: String
		minutes: Int
		tags: [String]
		n_steps: Int
		steps: [String]
		description: String
		ingredients: [String]
		n_ingredients: Int
	}

	type User {
		_id: ID!
		username: String!
		createdAt: String!
		token: String!
	}

	type Search {
		_id: ID!
		searchValue: String!
		createdAt: String!
		userID: String!
		username: String!
	}

	input RegisterInput {
		username: String!
		password: String!
		confirmPassword: String!
	}

	type Query {
		users: [User]
		authenticated: User
		getUser(userID: String!): User
		getSearchHistory(userID: ID): [Search]
		getSearch(searchID: ID!): String
		recipe(recipeID: String): Recipe
		closestRecipe(
			name: String
			description: String
			minutes: Int
			tags: [String]
			ingredients: [String]
			steps: [String]
		): Recipe
		recipes(
			searchValue: String
			start: Int
			limit: Int
			sortField: String
			sortDESC: Boolean
			minMinutes: Int
			maxMinutes: Int
			minIngredients: Int
			maxIngredients: Int
			minSteps: Int
			maxSteps: Int
		): [Recipe]
		wordcloud: [CountedWord]
	}

	type Mutation {
		createRecipe(
			name: String
			minutes: Int
			tags: [String]
			n_steps: Int
			steps: [String]
			description: String
			ingredients: [String]
			n_ingredients: Int
		): Recipe
		deleteRecipe(recipeID: String): Boolean
		register(registerInput: RegisterInput): User!
		login(username: String!, password: String!): User!
		addHistory(searchValue: String!): Search!
	}

	schema {
		query: Query
		mutation: Mutation
	}
`;
