const userResolvers = require('./users')
const recipeResolvers = require('./recipes')
const searchResolvers = require('./search')

module.exports = {
    Query: {
        ...recipeResolvers.Query,

        // "Login" is implemented as a mutation even though it does not modify anything; this is done out of convention.
        ...userResolvers.Query,

        ...searchResolvers.Query
    },
    Mutation: {
        ...recipeResolvers.Mutation,

        ...userResolvers.Mutation,

        ...searchResolvers.Mutation
    }
}