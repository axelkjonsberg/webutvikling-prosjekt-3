const bcrypt = require("bcryptjs");
const jsonWebToken = require("jsonwebtoken");
const FormatError = require("easygraphql-format-error");

const { SECRET_KEY } = require("../../../config");
const User = require("../../models/User");
const authenticateUser = require("../../authenticateUser");

//TODO: Better implementation of easygraphql-format-error

// Generate token from SECRET_KEY which the user can use to authenticate with.
function generateToken(user) {
	const token = jsonWebToken.sign(
		{
			_id: user._id,
			username: user.username,
		},
		SECRET_KEY,
		{ expiresIn: "2h" }
	);
	return token;
}

module.exports = {
	Query: {
		users: async () => {
			const users = await User.find({ username: { $ne: null } });
			return users;
		},
		getUser: async (_, { userID }) => {
			try {
				const user = await User.findById(userID);
				if (user) {
					return user;
				} else {
					throw new Error("User not found");
				}
			} catch (error) {
				throw new Error(error);
			}
		},
		authenticated: async (_, args, context) => {
			return authenticateUser(context);
		},
	},
	Mutation: {
		async login(_, { username, password }) {
			const loginErrors = {};

			const user = await User.findOne({ username });
			if (username.trim() === "") {
				loginErrors.username = new FormatError([
					{
						name: "Username",
						message: "Username cannot be empty",
						statusCode: "400", // The status codes are currently just for show, and do not have any real meaning
					},
				]);
			}

			if (!user) {
				loginErrors.existingUser = new FormatError([
					{
						name: "Existing user",
						message: "User allready exists",
						statusCode: "401",
					},
				]);
			}

			const match = await bcrypt.compare(password, user.password);
			if (!match) {
				loginErrors.credentials = new FormatError([
					{
						name: "Credentials",
						message: "Wrong credentials",
						statusCode: "402",
					},
				]);
			}

			for (let error of Object.entries(loginErrors)) {
				throw new Error(error);
			}

			const token = generateToken(user);
			return {
				...user._doc,
				id: user._id,
				token,
			};
		},
		async register(
			_,
			{
				registerInput: { username, password, confirmPassword },
			}
		) {
			// Validate user input data
			const registerErrors = {};

			if (username.trim() === "") {
				registerErrors.username = new FormatError([
					{
						name: "Username",
						message: "Username cannot be empty",
						statusCode: "400",
					},
				]);
			}

			if (password === "") {
				registerErrors.password = new FormatError([
					{
						name: "Password",
						message: "Password cannot be empty",
						statusCode: "400",
					},
				]);
			} else if (password !== confirmPassword) {
				registerErrors.confirmPassword = new FormatError([
					{
						name: "Confirm password",
						message: "Passwords do not match",
						statusCode: "405",
					},
				]);
			}

			// Make sure user doesn't already exist
			const userExists = await User.findOne({ username });
			if (userExists) {
				registerErrors.usernameTaken = new FormatError([
					{
						name: "Username taken",
						message: "The username is taken",
						statusCode: "406",
					},
				]);
			}

			for (let error of Object.entries(registerErrors)) {
				throw new Error(error);
			}

			// Hash password
			password = await bcrypt.hash(password, 12);

			// Save the new user in database
			const newUser = new User({
				username,
				password,
				createdAt: new Date().toISOString(),
			});
			const response = await newUser.save();

			// Generate security token so that only the connected server can decode the data
			const token = generateToken(response);

			return {
				...response._doc,
				id: response._id,
				token,
			};
		},
	},
};
