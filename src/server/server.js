const mongoose = require("mongoose");
const express = require('express');
const { ApolloServer } = require('apollo-server-express');
const cors = require("cors");

const { MONGO_CONNECTIONSTRING } = require("../config.js");
const typeDefs = require("./graphql/typeDefs");
const resolvers = require("./graphql/resolvers");

const start = async HOST => {
	try {
		const port = 4000;
		let URL = `http://it2810-25.idi.ntnu.no:${port}`;
		if (HOST) {
			URL = "http://localhost:" + port
		}
		console.log(URL);
		await mongoose.connect(MONGO_CONNECTIONSTRING, {
			useUnifiedTopology: true,
			useNewUrlParser: true,
		});

		const server = new ApolloServer({
		    typeDefs,
		    resolvers,
		  	context: ({ req }) => ({ req }),
		});
		
		const app = express();
		app.use(cors());
		server.applyMiddleware({ app });
		
		app.listen(port, () => {
			console.log(`Visit ${URL}${server.graphqlPath}`);
		});
	} catch (error) {
		console.error(error);
	}
};

module.exports = start;
