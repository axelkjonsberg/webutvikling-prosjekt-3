const { model, Schema } = require("mongoose");

const recipeSchema = new Schema({
	name: String,
	minutes: Number,
	tags: [String],
	n_steps: Number,
	steps: [String],
	description: String,
	ingredients: [String],
	n_ingredients: Number,
});

module.exports = model("Recipe", recipeSchema);
