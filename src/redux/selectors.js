export const getRecipeFilters = store => store.recipeFilters;

export const getRecipeSearchValue = store => store.recipeSearch.searchValue;

export const getRecipeSort = store => store.recipeSort;

export const getRecipeLimit = store => store.recipeLimit.limit;

export const getRecipePage = store => store.recipePage.page;

export const getSearchCriteria = store => {
	const searchValue = getRecipeSearchValue(store);
	const sort = getRecipeSort(store);
	const filters = getRecipeFilters(store);
	const limit = getRecipeLimit(store);
	const start = getRecipePage(store);
	return {
		searchValue: searchValue,
		limit: limit,
		start: start * limit,
		sortField: sort.sortField,
		sortDESC: sort.sortDESC,
		minMinutes: filters.minMinutes,
		maxMinutes: filters.maxMinutes,
		minIngredients: filters.minIngredients,
		maxIngredients: filters.maxIngredients,
		minSteps: filters.minSteps,
		maxSteps: filters.maxSteps,
	};
};

export const getRecipes = store => store.recipes.recipes;
