import React from "react";
import ReactDOM from "react-dom";

import { Provider } from "react-redux";
import store from "./redux/store";

import "./materialImports";
import "./index.scss";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { RMWCProvider } from "@rmwc/provider";
import { ThemeProvider } from "@rmwc/theme";

ReactDOM.render(
	<RMWCProvider>
		<ThemeProvider
			options={{ primary: "#3949AB", secondary: "#3949AB", onSurface: "black" }}
			className='theme-provider'
		>
			<Provider store={store}>
				<App />
			</Provider>
		</ThemeProvider>
	</RMWCProvider>,
	document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
