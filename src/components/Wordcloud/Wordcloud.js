import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import ReactWordCloud from "react-wordcloud";
import { connect } from "react-redux";
import { Typography } from "@rmwc/typography";

import { fetchWordcloud } from "../../server/queries";
import { setLoading, setSearchValue, resetPage } from "../../redux/actions";
import "./Wordcloud.scss";

const Wordcloud = props => {
	const [words, setWords] = useState([]);

	const history = useHistory();

	useEffect(() => {
		props.dispatch(setLoading(true));
		if (sessionStorage.getItem("wordcloud")) {
			setWords(JSON.parse(sessionStorage.getItem("wordcloud")));
			props.dispatch(setLoading(false));
		} else {
			fetchWordcloud()
				.then(res => {
					const wordsFormatted = res.data.wordcloud.map(word => {
						return { text: word.word, value: word.count };
					});
					sessionStorage.setItem("wordcloud", JSON.stringify(wordsFormatted));
					setWords(wordsFormatted);
				})
				.catch(error => console.error(error))
				.finally(() => props.dispatch(setLoading(false)));
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	function handleWordClick(wordObject) {
		props.dispatch(setSearchValue(wordObject.text));
		props.dispatch(resetPage());
		history.push("/prosjekt3/");
	}

	return (
		<div className='wordcloud'>
			<Typography use='headline4' className='headline'>
				Most used words in recipe names
			</Typography>
			{!props.loading && words ? (
				<div className='wordcloud-svg'>
					<ReactWordCloud
						words={words}
						options={{ fontFamily: "Roboto", fontSizes: [24, 64] }}
						callbacks={{ onWordClick: handleWordClick }}
					/>
				</div>
			) : (
				<div></div>
			)}
		</div>
	);
};

const mapStateToProps = state => ({
	loading: state.loading.loading,
});

export default connect(mapStateToProps)(Wordcloud);
