import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { Typography } from "@rmwc/typography";
import { TextField } from "@rmwc/textfield";
import { Button } from "@rmwc/button";
import { IconButton } from "@rmwc/icon-button";

import "./Signup.scss";
import { registerUser } from "../../server/queries";
import { setUser, setLoading } from "../../redux/actions";
import { connect } from "react-redux";

const Signup = props => {
	const [username, setUsername] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");
	const [validUsername, setValidUsername] = useState(true);
	const [validPassword, setValidPassword] = useState(true);
	const [errorMessage, setErrorMessage] = useState();

	const history = useHistory();

	function handleSignup() {
		const safeUsername = username.replace("\\", "\\\\");
		const safePassword = password.replace("\\", "\\\\");
		const safeConfirmPassword = confirmPassword.replace("\\", "\\\\");
		props.dispatch(setLoading(true));
		registerUser(safeUsername, safePassword, safeConfirmPassword)
			.then(res => {
				if (res.data) {
					const login = res.data.register;
					localStorage.setItem("token", login.token);
					localStorage.setItem("userID", login._id);
					localStorage.setItem("username", login.username);
					props.dispatch(setUser(login._id, login.token, login.username));
					history.push("/prosjekt3/");
				} else {
					if (
						res.errors.some(error => error.message.includes("usernameTaken"))
					) {
						setErrorMessage("That username is already taken");
					} else {
						console.log(res.errors[0]);
						setErrorMessage(res.errors[0].message);
					}
					props.dispatch(setLoading(false));
				}
			})
			.catch(error => {
				console.error(error);
				props.dispatch(setLoading(false));
			});
	}

	function validateUsername() {
		const isValid =
			username.length > 0 && (username.length >= 6 && username.length <= 64);
		setValidUsername(isValid);
	}

	function validatePassword() {
		const isValid = password.length >= 8;
		setValidPassword(isValid);
	}

	function isValid() {
		return (
			username && password && confirmPassword && password === confirmPassword && validUsername && validPassword
		);
	}

	return (
		<div className='signup'>
			<div className='actions'>
				<Link to='/prosjekt3/'>
					<IconButton icon='arrow_back' />
				</Link>
			</div>
			<div className='signup-form'>
				<div className='signup-title'>
					<Typography use='headline3' className='headline'>
						Sign up
					</Typography>
				</div>
				{errorMessage ? (
					<div className='signup-error-message'>
						<Typography use='caption'>{errorMessage}</Typography>
					</div>
				) : (
					<div></div>
				)}
				<div className='signup-field'>
					<TextField className ='signup-username'
						outlined
						required
						invalid={!validUsername}
						helpText={{
							validationMsg: true,
							children: "Must be between 6 and 64 characters",
						}}
						maxLength={64}
						minLength={6}
						characterCount
						label='Username'
						value={username}
						onBlur={() => validateUsername()}
						onChange={e => setUsername(e.target.value)}
					></TextField>
				</div>
				<div className='signup-field'>
					<TextField className ='signup-password'
						outlined
						required
						invalid={!validPassword}
						helpText={{
							validationMsg: true,
							children: "Needs at least 8 characters",
						}}
						type='password'
						label='Password'
						value={password}
						onBlur={() => validatePassword()}
						onChange={e => setPassword(e.target.value)}
					></TextField>
				</div>
				<div className='signup-field'>
					<TextField className ='signup-confirm'
						outlined
						required
						type='password'
						label='Confirm password'
						value={confirmPassword}
						onChange={e => setConfirmPassword(e.target.value)}
					></TextField>
				</div>
				<div className='submit-button-wrapper'>
					<Button raised onClick={() => handleSignup()} disabled={!isValid()}>
						Sign up
					</Button>
				</div>
			</div>
		</div>
	);
};

const mapStateToProps = state => ({});

export default connect(mapStateToProps)(Signup);
