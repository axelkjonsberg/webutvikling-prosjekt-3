import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { IconButton } from "@rmwc/icon-button";
import { Typography } from "@rmwc/typography";
import { TextField } from "@rmwc/textfield";
import { Button } from "@rmwc/button";

import "./CreateRecipe.scss";
import { Icon } from "@rmwc/icon";
import { createRecipe, getClosestRecipeID } from "../../server/queries";

const CreateRecipe = props => {
	const [name, setName] = useState();
	const [minutes, setMinutes] = useState();
	const [description, setDescription] = useState("");
	const [ingredients, setIngredients] = useState([]);
	const [steps, setSteps] = useState([]);
	const [tags, setTags] = useState([]);

	const history = useHistory();

	async function handleSubmit() {
		const formResult = {
			name,
			minutes,
			description,
			ingredients,
			steps,
			tags,
		};
		const result = await createRecipe(formResult, { _id: true, name: true });
		if (result.error) {
			console.error(result);
		} else {
			const recipeID = await getClosestRecipeID(formResult);
			if (recipeID && recipeID.data && recipeID.data.closestRecipe) {
				history.push("/prosjekt3/recipes/" + recipeID.data.closestRecipe._id);
			} else {
				history.push("/prosjekt3/");
			}
		}
	}

	function isFormInvalid() {
		return !(name && minutes);
	}

	function handleChipAdd(event, chipType) {
		if (event.key === "Enter") {
			const input = event.target.value.trim();
			if (chipType === "ingredient") {
				const concatIngredients = ingredients.concat(input);
				setIngredients(concatIngredients);
			} else if (chipType === "step") {
				const concatSteps = steps.concat(input);
				setSteps(concatSteps);
			} else if (chipType === "tag") {
				const concatTags = tags.concat(input);
				setTags(concatTags);
			}
			event.target.value = "";
		}
	}

	function handleChipRemove(chipValue, chipType) {
		if (chipType === "ingredient") {
			if (ingredients.includes(chipValue)) {
				const ingredientIndex = ingredients.findIndex(
					value => value === chipValue
				);
				const filteredIngredients = ingredients.map(value => value);
				filteredIngredients.splice(ingredientIndex, 1);
				setIngredients(filteredIngredients);
			}
		} else if (chipType === "step") {
			if (steps.includes(chipValue)) {
				const stepIndex = steps.findIndex(value => value === chipValue);
				const filteredSteps = steps.map(value => value);
				filteredSteps.splice(stepIndex, 1);
				setSteps(filteredSteps);
			}
		} else if (chipType === "tag") {
			if (tags.includes(chipValue)) {
				const tagIndex = tags.findIndex(value => value === chipValue);
				const filteredTags = tags.map(value => value);
				filteredTags.splice(tagIndex, 1);
				setTags(filteredTags);
			}
		}
	}

	return (
		<div className='create-recipe'>
			<div className='top-actions'>
				<Link to='/prosjekt3/'>
					<IconButton icon='arrow_back' className='back-button' />
				</Link>
				<div className='horizontal-filler'></div>
			</div>
			<div className='recipe-name'>
				<Typography use='headline3' className='headline'>
					Create a recipe
				</Typography>
			</div>
			<div className='recipe-form'>
				<div className='recipe-form-field two-fields'>
					<div className='recipe-name-form'>
						<TextField
							outlined
							style={{ width: "100%" }}
							label='Name of recipe'
							required
							characterCount
							maxLength={50}
							value={name}
							onChange={e => setName(e.target.value)}
						/>
					</div>
					<div className='recipe-minutes-form'>
						<TextField
							outlined
							label='Minutes to make'
							required
							type='number'
							min={1}
							max={100000}
							value={minutes}
							onChange={e => setMinutes(e.target.value)}
						/>
					</div>
				</div>
				<div className='recipe-form-field'>
					<TextField
						outlined
						textarea
						label='Description'
						value={description}
						onChange={e => setDescription(e.target.value)}
					/>
				</div>
				<div className='recipe-form-field'>
					<Typography use='subtitle1' className='headline'>
						Ingredients
					</Typography>
					<div className='chip-list'>
						{ingredients.length ? (
							ingredients.map((ingredient, index) => (
								<div className='chip' key={index}>
									<Typography use='body1'>{ingredient}</Typography>
									<Icon
										icon={{ icon: "cancel", size: "xsmall" }}
										className='chip-icon'
										onClick={() => {
											handleChipRemove(ingredient, "ingredient");
										}}
									/>
								</div>
							))
						) : (
							<Typography use='body1'>
								No ingredients have been added yet
							</Typography>
						)}
					</div>
					<div>
						<TextField
							outlined
							icon='add'
							label='Add an ingredient'
							onKeyUp={e => {
								handleChipAdd(e, "ingredient");
							}}
						/>
					</div>
				</div>
				<div className='recipe-form-field'>
					<Typography use='subtitle1' className='headline'>
						Steps
					</Typography>
					<div className='chip-list'>
						{steps.length ? (
							<ol>
								{steps.map((step, index) => (
									<li key={index}>
										<div className='chip'>
											<Typography use='body1'>{step}</Typography>
											<Icon
												icon={{ icon: "cancel", size: "xsmall" }}
												className='chip-icon'
												onClick={() => {
													handleChipRemove(step, "step");
												}}
											/>
										</div>
									</li>
								))}
							</ol>
						) : (
							<Typography use='body1'>No steps have been added yet</Typography>
						)}
					</div>
					<div>
						<TextField
							outlined
							icon='add'
							label='Add a step'
							onKeyUp={e => {
								handleChipAdd(e, "step");
							}}
						/>
					</div>
				</div>
				<div className='recipe-form-field'>
					<Typography use='subtitle1' className='headline'>
						Tags
					</Typography>
					<div className='chip-list'>
						{tags.length ? (
							tags.map((tag, index) => (
								<div className='chip' key={index}>
									<Typography use='body1'>{tag}</Typography>
									<Icon
										icon={{ icon: "cancel", size: "xsmall" }}
										className='chip-icon'
										onClick={() => {
											handleChipRemove(tag, "tag");
										}}
									/>
								</div>
							))
						) : (
							<Typography use='body1'>No tags have been added yet</Typography>
						)}
					</div>
					<div>
						<TextField
							outlined
							icon='add'
							label='Add a tag'
							onKeyUp={e => {
								handleChipAdd(e, "tag");
							}}
						/>
					</div>
				</div>
				<div className='submit-button'>
					<Button
						disabled={isFormInvalid()}
						onClick={() => handleSubmit()}
						raised
					>
						Create recipe
					</Button>
				</div>
			</div>
		</div>
	);
};
export default CreateRecipe;
