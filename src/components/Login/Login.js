import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { Typography } from "@rmwc/typography";
import { TextField } from "@rmwc/textfield";
import { Button } from "@rmwc/button";
import { IconButton } from "@rmwc/icon-button";

import "./Login.scss";
import { loginUser } from "../../server/queries";
import { setUser, setLoading } from "../../redux/actions";
import { connect } from "react-redux";

const Login = props => {
	const [username, setUsername] = useState();
	const [password, setPassword] = useState();
	const [errorMessage, setErrorMessage] = useState();

	const history = useHistory();

	if (props.token) {
		history.push("/prosjekt3/");
	}

	function handleLogin() {
		const safeUsername = username.replace("\\", "\\\\");
		const safePassword = password.replace("\\", "\\\\");
		props.dispatch(setLoading(true));
		loginUser(safeUsername, safePassword)
			.then(res => {
				if (res.data) {
					const login = res.data.login;
					localStorage.setItem("token", login.token);
					localStorage.setItem("userID", login._id);
					localStorage.setItem("username", login.username);
					props.dispatch(setUser(login._id, login.token, login.username));
					history.push("/prosjekt3/");
				} else {
					if (
						res.errors.some(
							error =>
								error.message.includes("credentials") ||
								error.message.includes("password")
						)
					) {
						setErrorMessage("Wrong username or password. Please try again.");
					} else {
						console.error(res.errors);
						setErrorMessage("Something went wrong, please try again.");
					}
					setUsername("");
					setPassword("");
					props.dispatch(setLoading(false));
				}
			})
			.catch(error => {
				console.error(error);
				props.dispatch(setLoading(false));
			});
	}

	function isValid() {
		return username && password;
	}

	return (
		<div className='login'>
			<div className='actions'>
				<Link to='/prosjekt3/'>
					<IconButton icon='arrow_back' />
				</Link>
			</div>
			<div className='login-form'>
				<div className='login-title'>
					<Typography use='headline3' className='headline'>
						Log in
					</Typography>
				</div>
				{errorMessage ? (
					<div className='login-error-message'>
						<Typography use='caption'>{errorMessage}</Typography>
					</div>
				) : (
					<div></div>
				)}
				<div className='login-field'>
					<TextField
						className='login-username'
						outlined
						required
						label='Username'
						value={username}
						onChange={e => setUsername(e.target.value)}
					></TextField>
				</div>
				<div className='login-field'>
					<TextField
						className='login-password'
						outlined
						required
						type='password'
						label='Password'
						value={password}
						onChange={e => setPassword(e.target.value)}
					></TextField>
				</div>
				<div className='submit-button-wrapper'>
					<Button className="login-button" raised onClick={handleLogin} disabled={!isValid()}>
						Log in
					</Button>
				</div>
			</div>
		</div>
	);
};

const mapStateToProps = state => ({
	token: state.user.token,
});

export default connect(mapStateToProps)(Login);
