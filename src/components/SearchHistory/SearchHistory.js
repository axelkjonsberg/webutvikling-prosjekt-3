import React, { useState, useEffect } from "react";
import { Typography } from "@rmwc/typography";
import { List } from "@rmwc/list";

import "./SearchHistory.scss";
import { connect } from "react-redux";
import HistoryEntry from "./HistoryEntry";
import { getSearchHistory } from "../../server/queries";
import { useHistory } from 'react-router-dom';

const SearchHistory = props => {
	const [entries, setEntries] = useState([]);

	const history = useHistory()

	useEffect(() => {
		if (!props.userID) {
			history.push("/prosjekt3/");
		}
		if (props.userID) {
			getSearchHistory(props.userID).then(res => {
				if (res.data) {
					setEntries(res.data.getSearchHistory);
				} else {
					console.log(res.error);
				}
			})
			.catch(error => console.error(error));
		}
	}, [props.userID, history])


	const now = new Date();

	return (
		<div className='search-history'>
			<div>
				<Typography use='headline4' className='headline'>
					Your search history
				</Typography>
			</div>
			{entries.length ? (<div>
				<List>
					{entries.map((entry, index) => (
						<HistoryEntry entry={entry.searchValue} timestamp={entry.createdAt} now={now} key={index} />
					))}
				</List>
			</div>) : (
				<div><Typography use="body1">You have no search history yet. Go look up some recipes!</Typography></div>
			)}
		</div>
	);
};

const mapStateToProps = state => ({
	userID: state.user.id
});

export default connect(mapStateToProps)(SearchHistory);
