import React from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import {
	ListItem,
	ListItemText,
	ListItemPrimaryText,
	ListItemSecondaryText,
} from "@rmwc/list";

import "./SearchResult.scss";
import { formatMinutes } from '../Recipe/formatUtils';

const SearchResult = props => {
	return (
		<Link to={"/prosjekt3/recipes/" + props.result._id}>
			<ListItem>
				<ListItemText>
					<ListItemPrimaryText style={{ textTransform: "capitalize" }}>
						{props.result.name}
					</ListItemPrimaryText>
					<ListItemSecondaryText>
						<span>{formatMinutes(props.result.minutes)}</span>
					</ListItemSecondaryText>
				</ListItemText>
			</ListItem>
		</Link>
	);
};
export default SearchResult;

SearchResult.propTypes = {
	result: PropTypes.shape({
		_id: PropTypes.string,
		name: PropTypes.string,
		minutes: PropTypes.number,
	}),
};
