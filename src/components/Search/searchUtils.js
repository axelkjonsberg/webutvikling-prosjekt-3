import { searchRecipes } from "../../server/queries";

export default async function getRecipeList(search) {
	const recipes = await searchRecipes(search, {
		_id: true,
		name: true,
		minutes: true,
	});
	return recipes.data.recipes;
}
