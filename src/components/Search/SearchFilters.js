import React, { useState } from "react";
import { Slider } from "@rmwc/slider";
import { Typography } from "@rmwc/typography";

import "./SearchFilters.scss";
import { connect } from "react-redux";
import {
	setMaxIngredients,
	setMaxMinutes,
	setMaxSteps,
	resetPage,
} from "../../redux/actions";
import { getRecipeSearchValue } from "../../redux/selectors";

const SearchFilters = props => {
	const [localMaxIngredients, setLocalMaxIngredients] = useState(
		props.maxIngredients
	);
	const [localMaxMinutes, setLocalMaxMinutes] = useState(props.maxMinutes);
	const [localMaxSteps, setLocalMaxSteps] = useState(props.maxSteps);

	return (
		<div className='search-filters'>
			<div style={{ marginBottom: "16px" }}>
				<Typography use='headline6' className='headline'>
					Filters
				</Typography>
			</div>
			<div>
				<Typography use='subtitle1'>Max number of ingredients</Typography>
				<Slider
					discrete
					displayMarkers
					min={0}
					max={50}
					step={1}
					value={localMaxIngredients}
					onInput={e => {
						setLocalMaxIngredients(e.detail.value);
					}}
					onChange={() => {
						props.dispatch(setMaxIngredients(localMaxIngredients));
						props.dispatch(resetPage());
					}}
					disabled={!props.searchValue.length}
				/>
			</div>
			<div>
				<Typography use='subtitle1'>Max prep time (mins)</Typography>
				<Slider
					discrete
					displayMarkers
					min={0}
					max={240}
					step={2}
					value={localMaxMinutes}
					onInput={e => {
						setLocalMaxMinutes(e.detail.value);
					}}
					onChange={() => {
						props.dispatch(setMaxMinutes(localMaxMinutes));
						props.dispatch(resetPage());
					}}
					disabled={!props.searchValue.length}
				/>
			</div>
			<div>
				<Typography use='subtitle1'>Max number of steps</Typography>
				<Slider
					discrete
					displayMarkers
					min={0}
					max={100}
					step={1}
					value={localMaxSteps}
					onInput={e => {
						setLocalMaxSteps(e.detail.value);
					}}
					onChange={() => {
						props.dispatch(setMaxSteps(localMaxSteps));
						props.dispatch(resetPage());
					}}
					disabled={!props.searchValue.length}
				/>
			</div>
		</div>
	);
};

const mapStateToProps = state => ({
	maxIngredients: state.recipeFilters.maxIngredients,
	maxSteps: state.recipeFilters.maxSteps,
	maxMinutes: state.recipeFilters.maxMinutes,
	searchValue: getRecipeSearchValue(state),
});

export default connect(mapStateToProps)(SearchFilters);
