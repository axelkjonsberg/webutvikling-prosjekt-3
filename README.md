# GooseGooseRun – An alternative search engine for food recipes

## For developers

In order to run this project locally:
* After cloning, run `npm install`
* If you're not on eduroam, make sure to use an NTNU vpn
* Run `npm start` and check your browser
* The backend can be experimented with at `localhost:4000/graphql`

### Project structure

Since the project was ejected in order to modify the start script, our project displays many files and dependencies that are normally hidden by React.
<br>
A tree for explaining the current project structure is displayed below:

**|-- config** : React-generated stuff <br>
**|-- public** : Assets available to react, index.html <br>
**|-- scripts** : React's scripts, including "start", which we've modified a bit <br>
**|-- src** : It's the source-folder in which the app resides<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**|-- components** : All top-level components in their own folders, each with their own sub-components, style sheets and helper files <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**|-- redux** : The store, actions, reducers, selectors and actiontypes Redux needs to work <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**|-- server** : Scripts which connects the graphql and frontend to the MongoDB <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**|-- graphql** : Resolvers and type definitions used by the graphql endpoint <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**|-- models** : Models and schemas used by Mongoose to communicate with the MongoDB <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**|-- authenticateUser.js** : Jwt verification of authorization token <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**|-- queries.js** : Premade queries for use in frontend <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**|-- serve.js** : Script for running the backend alone <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**|-- server.js** : Top-level start script for the backend <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**|-- index.scss** : Global styles <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**|-- materialImports.js** : Imports from rmwc packed away in their own file <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**|-- mixins.scss** : Sass mixins <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**|-- text.scss** : Global text styles <br>
**|-- .gitignore** <br>
**|-- LICENSE** <br>
**|-- package-lock.json** <br>
**|-- package.json** <br>
&nbsp;**\\- README.md** <br>
<br>

## Features
Here is an image showing what the app should look like for a signed in user:
![alt text](https://i.imgur.com/I4VXhL8.png "Default look")


### Searching for recipes

When visiting the page, this is the first feature you will see. When you search for something, everything that matches with its name, tags, ingredients or even description will be in the result. The result may be filtered by choosing a max number of steps to make, prep time, and number of ingredients. To narrow your search further, you may surround a phrase you want the recipes to include with quotes. You may also exclude words by adding a "-" in front of them.
The result can be sorted by name, prep time, number of ingredients or number of steps in ascending or descending order. If you're in an adventurous mood, you may scroll as far down as you want. The list will be loaded and expanded as you go.

If a recipe sounds interesting, you can click on it to read it.

### Creating a recipe

Press the large floating "+"-button to create a recipe. Now you can share your best recipes with everyone else!

### Wordcloud of all recipe names

Under the "Wordcloud"-tab, you can see which words are most common in the recipes. Click on one to search for it.

### Signup and login

If you want, you can create a user for GooseGooseRun. When you're logged in, your search history will be saved, and it is viewable under the "History"-tab. Your login session expires after two hours.

## Technology

### Frontend

#### React

All components are implemented as functional components in order to reduce boilerplate code, improve readability, and to encourage moving state to redux. Components that display what they fetch directly, and components with a lot of user input have been given local state.

#### Redux

Redux is used to keep track of the current search criteria, whether the page is loading something, and for keeping login info in state in order to not rely on sessionStorage too much. It is very useful for determining when components may perform sideeffects, since the props from state can be used as dependencies in useEffect (for primitive values) or useDeepCompareEffect (for objects and arrays).

We've chosen to define some selectors for getting composite state. Single value states are retrieved directly. The actions and actiontypes are defined in order to minimize mistakes when dispatching an action.

#### RMWC - React Material Web Components

In order to save time on frontend will still getting good usability, we've chosen to use RMWC in order to not have to make all presentational components from scratch. The reason we chose this UI-library is that it has good documentation and is easy to use and override.

#### Sass

As before, we've used Sass for writing stylesheets with a better syntax. This reduces repetition and makes it easier to achieve the right layout.

#### React Wordcloud

The wordcloud is shown using React Wordcloud, which uses D3.js to display our data. We chose this library because it is very simple to use and has good documentation. It is a bit performance heavy though, so it would be nice if we had found an alternative.

#### React Router DOM

In order to use routing in the project we've used React Router DOM. The reason we chose it is that it has good documentation with examples, and it has hooks for use with functional components. For instance, it was easy to use url parameters with it for viewing a recipe, checking the current url (for the tabs), or redirecting the user programmatically.

### Backend

#### GraphQL and Express

We've used GraphQL for creating an API for handling requests to our database. It defines what queries the frontend may use, what it may recieve, and resolvers for interacting with the database.

Express is used for running the backend server and applying everything GraphQL defines

#### Apollo-server-express

Apollo-server-express is used for streamlining the use of GraphQL with Express. It allows us to use an interface to test our queries and quickly discover errors with the typedefs or resolvers.

#### Mongoose

In order to execute queries on our MongoDB database, we've used Mongoose. We originally used MongoClient, but since it did not follow the standards of MongoDB and had poor documentation, we considered this as a better alternative. Mongoose makes all interaction with the database simple, and the documentation is easy to follow.

#### MongoDB

We've used MongoDB for our database because it handles large datasets very well. Since it uses an abstract query language, it is easy to use and understand. Complex queries take little time to implement, and error messages are easy to understand. Since it is popular, it is well documented and supported.

#### Token based authentication

Users are saved together with their encrypted password (using bcrypt) in the database. Upon registration or login, they are given a token to authenticate with. The token is generated using JSON web token and a very secret key (not the same SECRET_KEY as in our git-repo's config.js file).
Currently, the authentication is only used for giving the user access to their own search history, but it could also be used for features like "Adding a recipe to favourites".




## Getting enough data

In order to populate our database, we found an [open dataset on kaggle.com](https://www.kaggle.com/shuyangli94/food-com-recipes-and-user-interactions), parsed it and inserted it into our database. Citation is at the bottom, as they requested.

## Testing
The project includes both comprehensive end-to-end testing (with Cypress) and a few unit-tests. By using Cypress, we have attempted to test all of the relevant user functionality.

### Running tests
The best way to run the end-to-end tests would be to open the Cypress tool with your command line.
Make sure you have `node` and `npm` installed, then you can either use `npm run cypress` or `npx cypress run` to open Cypress.
From here, all tests can be run in succession or individually (we would recommend that you run them all in order).
<br><br>
If you would like to run the unit tests, run the command `npm test`
in your console terminal.

### Browsers tested

* Firefox v69
* Chrome v77
* Safari v12.1
* Edge v44

### Devices tested

* Macbook Air running MacOS
* Lenovo Yoga 520 running Kubuntu
* Lenovo Yoga 530 running Windows 10
* Assembled desktop running Windows 10

## Git usage and flow

We've used a protected master-branch and development-branch from the very beginning. Each issue gets its own branch. When it is completed, a merge request is created with a propper description, pictures and <b>references to the issues which it solves</b>. We chose not to reference the issues in each commit, since they allready can be linked to the relevant issues by referring to which branch they were made in. 

## Citations

### Food.com recipes and user interactions

Generating Personalized Recipes from Historical User Preferences

Bodhisattwa Prasad Majumder*, Shuyang Li*, Jianmo Ni, Julian McAuley

EMNLP, 2019