/* eslint-disable no-undef */
describe("Log in", () => {
	beforeEach(() => {
		localStorage.clear();
		cy.visit("/login");
	});

	it("accepts input", () => {
		cy.get(".login-username")
			.children("input")
			.type("text")
			.should("have.value", "text");
	});

	it('disables "Log in" button if requirements are not met', () => {
		cy.get(".login-button").should("have.disabled", "");

		cy.get(".login-username").type("Tester");

		cy.get(".login-button").should("have.disabled", "");
	});

	it("fails to login with wrong password", () => {
		cy.get(".login-username").type("Tester");

		cy.get(".login-password").type("wrong password");

		cy.get(".login-button").click();

		cy.get(".mdc-typography--caption").contains(
			"Wrong username or password. Please try again."
		);
	});

	it("properly logs in when correct credentials are given", () => {
		let token;
		let username;

		cy.get(".login-username").type("Tester");

		cy.get(".login-password").type("TestingYeh");

		cy.get(".login-button").click();

		cy.url()
			.should("eq", "http://localhost:3000/prosjekt3/")
			.then(() => {
				token = localStorage.getItem("token");
				username = localStorage.getItem("username");
			})
			.then(() => expect(token).to.not.be.null)
			.then(() => expect(username).to.equal("Tester"));
	});
});
