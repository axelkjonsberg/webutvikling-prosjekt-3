/* eslint-disable no-undef */
describe('View word in the wordcloud', () => {

    it('finds the "Wordcloud" tab', () => {
        cy.visit('/')
        cy.get('.wordcloud')
        .click({ force: true })
        cy.url()
        .should('eq', 'http://localhost:3000/prosjekt3/wordcloud')
    })

    it('finds a element in the cloud', () => {
        cy.get('svg')
        .children('g')
        .children('text')
        .first()
        .contains('chicken')
        
        cy.get('svg')
        .children('g')
        .children('text')
        .first()
        .click()
        .then(() => {
            cy.url()
            .should('eq', 'http://localhost:3000/prosjekt3/')
            .then(() => {
                cy.get('.searchfield').children('.searchfield')
                .children('input')
                .should('have.value', 'chicken')
            })
        })
    })
})