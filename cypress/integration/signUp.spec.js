/* eslint-disable no-undef */
describe("Sign up", () => {
	beforeEach(() => {
		localStorage.clear();
		cy.visit("/signup");
	});

	it("accepts input", () => {
		cy.get(".signup-username")
			.children()
			.first()
			.type("text")
			.should("have.value", "text");

		cy.get(".signup-password")
			.children()
			.first()
			.type("text")
			.should("have.value", "text");

		cy.get(".signup-confirm")
			.children()
			.first()
			.type("text")
			.should("have.value", "text");
	});

	it('disables "Sign up" button if requirements are not met', () => {
		cy.get(".submit-button-wrapper")
			.children()
			.should("have.disabled", "");

		cy.get(".signup-username").type("NewUser1234678");

		cy.get(".signup-password").type("<8");

		cy.get(".signup-confirm").type("<8");

		cy.get(".submit-button-wrapper")
			.children()
			.should("have.disabled", "");
	});

	it("fails to register existing user", () => {
		cy.get(".signup-username").type("Tester");

		cy.get(".signup-password").type("ThisCanBeAnything");

		cy.get(".signup-confirm").type("ThisCanBeAnything");

		cy.get(".submit-button-wrapper")
			.first()
			.click();

		cy.get(".signup-error-message")
			.get("span")
			.contains("That username is already taken");
	});
});
