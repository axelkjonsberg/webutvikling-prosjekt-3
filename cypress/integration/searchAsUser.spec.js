/* eslint-disable no-undef */
describe("Search as user and view history", () => {
	beforeEach(() => {
		cy.window().then(win => {
			win.localStorage.clear();
		});
		cy.visit("/login");
		cy.get(".login-username").type("Tester");
		cy.get(".login-password").type("TestingYeh");
		cy.get(".login-button").click();
		cy.wait(2000);
	});

	afterEach(() => {
		cy.window().then(win => {
			win.localStorage.clear();
		});
		cy.visit("/");
	});

	it("displays last search", () => {
		const firstSearch = "apple";
		const secondSearch = "quiche";

		cy.get(".searchfield")
			.children(".searchfield")
			.type(firstSearch)
			.type("{enter}");
		cy.wait(2000);
		cy.get(".tab-history").click({ force: true });
		cy.wait(2000);
		cy.get(".history-searchvalue")
			.first()
			.contains(firstSearch);

		cy.get(".tab")
			.first()
			.click({ force: true });
		cy.get(".searchfield")
			.children(".searchfield")
			.type("{selectall}{backspace}")
			.type(secondSearch)
			.type("{enter}");
		cy.wait(2000);
		cy.get(".tab-history").click({ force: true });
		cy.wait(2000);
		cy.get(".history-searchvalue")
			.first()
			.contains(secondSearch);
	});
});
